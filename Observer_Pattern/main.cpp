#include "Forms/Observer_Pattern.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	Observer_Pattern w;
	w.show();
	return a.exec();
}
