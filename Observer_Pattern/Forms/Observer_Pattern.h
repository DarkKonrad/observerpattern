#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Observer_Pattern.h"

class Observer_Pattern : public QMainWindow
{
	Q_OBJECT

public:
	Observer_Pattern(QWidget *parent = Q_NULLPTR);

private:
	Ui::Observer_PatternClass ui;
};
