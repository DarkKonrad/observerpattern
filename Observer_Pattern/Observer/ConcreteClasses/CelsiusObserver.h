#pragma once

#include "../Interfaces/iObserver.h"
#include "../Interfaces/iObservable.h"


namespace Observer
{
	namespace ConcreteClass
	{
		class CelsiusObserver : public Interface::iObserver
		{
			float celciusTemperature;
			virtual void update() override;

		};


	}
}
