#pragma once

#include "iObservable.h"

namespace Observer
{
	namespace Interface
	{
		class iObserver
		{
		public:
			virtual void update() = 0;
		};


	}
}
