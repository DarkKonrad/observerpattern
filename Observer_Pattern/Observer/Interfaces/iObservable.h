#pragma once
#include <qobject.h>
#include <vector>

namespace Observer 
{
	class iObserver;

	namespace Interface 
	{
		class iObservable
		{
		public:
			std::vector<iObserver*> observerCollection;
			virtual void attachObserver(iObserver* observer);
			virtual void detachObserver(iObserver* observer);
			virtual void removeAllObservers();
		
		protected:
			virtual void notifyObservers() =0;
		};
	}
}