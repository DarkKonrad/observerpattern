#include "iObservable.h"


using namespace Observer;
using namespace Observer::Interface;

void iObservable::attachObserver(iObserver* observer)
{
	this->observerCollection.push_back(observer);
}

void iObservable::detachObserver(iObserver* observer)
{
	for (auto it = this->observerCollection.begin(); it != this->observerCollection.end();it++)
	{
		if (*it == observer)
		{
			this->observerCollection.erase(it);
			break;
		}
	}
}

void iObservable::removeAllObservers()
{
	for (auto it = this->observerCollection.begin(); it != this->observerCollection.end(); it++)
	{		
		this->observerCollection.erase(it);
	}
}

